package com.ruoyi.common.core.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户注册对象
 * 
 * @author ruoyi
 */
@Data
@ApiModel(value = "UpdatePassword", description = "修改密码信息")
public class ForgetPasswordBody
{
    /**
     * 账号
     */
    @ApiModelProperty(value = "邮箱",required = true)
    private String email;

    /**
     * 新密码
     */
    @ApiModelProperty(value = "新密码",required = true)
    private String password;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码",required = true)
    private String code;
}
