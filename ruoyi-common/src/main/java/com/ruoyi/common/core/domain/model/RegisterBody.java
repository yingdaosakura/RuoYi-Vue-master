package com.ruoyi.common.core.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户注册对象
 * 
 * @author ruoyi
 */
@Data
@ApiModel(value = "RegisterBody", description = "注册信息")
public class RegisterBody
{

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称",required = true)
    private String nickname;

    /**
     * 账号
     */
    @ApiModelProperty(value = "邮箱",required = true)
    private String email;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",required = true)
    private String password;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码",required = true)
    private String code;
}
