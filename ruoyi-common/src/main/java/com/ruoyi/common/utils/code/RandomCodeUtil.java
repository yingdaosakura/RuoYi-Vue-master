package com.ruoyi.common.utils.code;

import java.util.Random;

/**
 * 生成随机验证码
 * @author yangyuhang
 */
public class RandomCodeUtil {

    public static String generateCode() {
        final int length=6;
        String chars ="0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(chars.length());
            sb.append(chars.charAt(index));
        }
        return sb.toString();
    }
}

