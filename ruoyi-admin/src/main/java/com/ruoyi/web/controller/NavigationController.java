package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.web.entity.Navigation;
import com.ruoyi.web.service.NavigationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author yangyuhang
 */
@Api(tags = "导航信息管理")
@RestController
@RequestMapping("/navigation")
public class NavigationController {
    @Autowired
    private NavigationService navigationService;

    @ApiOperation("获取导航列表")
    @GetMapping("/list")
    public R<List<Navigation>> userList() {
        return R.ok(navigationService.list());
    }

    @ApiOperation("获取导航详细")
    @ApiImplicitParam(name = "id", value = "导航ID", required = true, dataType = "int", paramType = "path", dataTypeClass = Integer.class)
    @GetMapping("/{id}")
    public R<Navigation> getUser(@PathVariable Integer id) {
        if (id!=null) {
            return R.ok(navigationService.getById(id));
        } else {
            return R.fail("导航不存在");
        }
    }

    @PreAuthorize("@ss.hasRole('admin')")
    @ApiOperation("新增导航")
    @PostMapping("/save")
    public R<Boolean> save(@RequestBody Navigation navigation) {
        return R.ok(navigationService.save(navigation));
    }

    @PreAuthorize("@ss.hasRole('admin')")
    @ApiOperation("更新导航")
    @PutMapping("/update")
    public R<Boolean> update(@RequestBody Navigation navigation) {
        if (StringUtils.isNull(navigation) || StringUtils.isNull(navigation.getId())) {
            return R.fail("导航ID不能为空");
        }
        return R.ok(navigationService.updateById(navigation));
    }

    @PreAuthorize("@ss.hasRole('admin')")
    @ApiOperation("删除导航信息")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "int", paramType = "path", dataTypeClass = Integer.class)
    @DeleteMapping("/{userId}")
    public R<String> delete(@PathVariable Integer userId) {
        if (navigationService.getById(userId)!=null) {
            navigationService.removeById(userId);
            return R.ok();
        } else {
            return R.fail("用户不存在");
        }
    }

    @PreAuthorize("@ss.hasRole('admin')")
    @ApiOperation("上传导航图片")
    @ApiImplicitParam(name = "file", value = "图片文件", required = true)
    @PostMapping("/file")
    public R<String> uploadFileAvatar(MultipartFile file){
       return R.ok(navigationService.uploadFileAvatar(file));
    }
}
