package com.ruoyi.web.controller.tool;
 
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
 
/**
 * 自动配置类
 * @author wuKeFan
 * @date 2023-02-10 15:34:01
 */
@Configuration
@EnableConfigurationProperties(OpenAiProperties.class)
public class OpenAiAutoConfiguration {
}