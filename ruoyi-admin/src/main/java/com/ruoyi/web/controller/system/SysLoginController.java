package com.ruoyi.web.controller.system;
import java.util.List;
import java.util.Set;
import com.ruoyi.common.core.domain.model.RegisterBody;
import com.ruoyi.common.core.domain.model.ForgetPasswordBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.system.service.ISysMenuService;
import javax.annotation.security.PermitAll;

/**
 * 登录验证
 *
 * @author ruoyi
 */
@Api(tags = "登录注册管理",value = "登录注册Controller")
@RestController
public class SysLoginController {
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @ApiOperation("登录方法")
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody) {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 注册方法
     * @param registerBody 登录信息
     * @return 结果
     */

    @PermitAll
    @ApiOperation("注册方法")
    @PostMapping("/register")
    public AjaxResult register(@RequestBody RegisterBody registerBody) {
        AjaxResult ajax = AjaxResult.success(loginService.register(registerBody));
        return ajax;
    }

    /**
     * 注册发送邮件
     * @param email
     * @return
     */

    @PermitAll
    @ApiOperation("注册获取邮箱验证码")
    @PostMapping("/sendEmail")
    public void sendEmail(String email) {
        loginService.sendEmail(email);
    }


    /**
     * 找回密码发送邮件
     * @param email
     * @return
     */

    @PermitAll
    @ApiOperation("找回密码获取邮箱验证码")
    @PostMapping("/findSendEmail")
    public void findSendEmail(String email) {
        loginService.findSendEmail(email);
    }

    /**
     * 找回密码发送邮件
     * @param forgetPasswordBody
     * @return
     */

    @PermitAll
    @ApiOperation("忘记密码")
    @PutMapping("/forgetPassword")
    public AjaxResult forgetPassword(@RequestBody ForgetPasswordBody forgetPasswordBody) {
        AjaxResult ajax = AjaxResult.success(loginService.updatePassword(forgetPasswordBody));
        return ajax;
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo() {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public AjaxResult getRouters() {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(userId);
        return AjaxResult.success(menuService.buildMenus(menus));
    }
}
