package com.ruoyi.web.controller;

import javax.servlet.http.HttpServletRequest;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.web.controller.tool.OpenAiUtils;
import com.ruoyi.web.entity.ChatgptConversation;
import com.ruoyi.web.service.ChatGptService;
import com.theokanning.openai.completion.CompletionChoice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * @author yangyuhang
 */
@Api(tags = "ChatGpt管理",value = "ChatGpt管理")
@RestController
@RequestMapping("/ChatGpt")
public class ChatGptController {

    @Autowired
    private ChatGptService chatGptService;

    @ApiOperation("询问chat问题")
    @ApiImplicitParam(name = "msg", value = "问题", required = true)
    @GetMapping("/msg")
    public  R<Object> chat(HttpServletRequest req,String msg) {
        return R.ok(chatGptService.chat(req,msg));
    }

//    @ApiOperation("询问chat问题test")
//    @ApiImplicitParam(name = "msg", value = "问题", required = true)
//    @GetMapping("/msg/test")
    public R<String> test(String msg) {
        List<CompletionChoice> questionAnswer = OpenAiUtils.getSummarize(msg);
        for (CompletionChoice completionChoice : questionAnswer) {
            System.out.println(completionChoice.getText());
        }
        return R.ok(questionAnswer.get(0).getText());
    }

    @ApiOperation("查询用户的问题与回复")
    @GetMapping("/query")
    public  R<List<ChatgptConversation>> query(HttpServletRequest req) {
        return R.ok(chatGptService.query(req));
    }

}
