package com.ruoyi.web.entity;
 
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
/**
 * @author wuKeFan
 * @date 2023-02-10 15:40:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenAi {
 
  private String id;

 private String name;

 private String desc;

 private String model;

  // 提示模板
 private String prompt;

  // 创新采样
 private Double temperature;

  // 情绪采样
 private Double topP;

  // 结果条数
 private Double n = 1d;

  // 频率处罚系数
 private Double frequencyPenalty;

  // 重复处罚系数
 private Double presencePenalty;

  // 停用词
 private String stop;
 
}