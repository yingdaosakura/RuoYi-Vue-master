package com.ruoyi.web.mapper;

import com.ruoyi.web.entity.Navigation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yangyuhang
* @description 针对表【navigation】的数据库操作Mapper
* @createDate 2023-04-26 11:14:35
* @Entity com.ruoyi.web.entity.Navigation
*/
public interface NavigationMapper extends BaseMapper<Navigation> {

}




