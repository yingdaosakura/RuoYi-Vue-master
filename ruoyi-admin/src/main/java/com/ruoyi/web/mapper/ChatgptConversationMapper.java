package com.ruoyi.web.mapper;

import com.ruoyi.web.entity.ChatgptConversation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yangyuhang
* @description 针对表【chatgpt_conversation】的数据库操作Mapper
* @createDate 2023-05-03 19:13:05
* @Entity com.ruoyi.web.entity.ChatgptConversation
*/
public interface ChatgptConversationMapper extends BaseMapper<ChatgptConversation> {

}




