package com.ruoyi.web.service.impl;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.web.entity.ChatgptConversation;
import com.ruoyi.web.service.ChatGptService;
import com.ruoyi.web.service.ChatgptConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChatGptServiceImpl implements ChatGptService {

    @Autowired
    private ChatgptConversationService chatgptConversationService;

    @Autowired
    private TokenService tokenService;
    // 令牌自定义标识
    @Value("${token.header}")
    private String header;
    @Override
    public Object chat(HttpServletRequest req,String msg) {
        //获取当前登录用户
        LoginUser loginUser = tokenService.getLoginUser(req);
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json;charset=UTF-8");

        JSONObject json = new JSONObject();
        //选择模型
        json.set("model", "gpt-3.5-turbo");
        com.alibaba.fastjson2.JSONObject[] arr=new com.alibaba.fastjson2.JSONObject[1];
        com.alibaba.fastjson2.JSONObject jsonObject = new com.alibaba.fastjson2.JSONObject();
        jsonObject.put("role","user");
        jsonObject.put("content",msg);
        arr[0]=jsonObject;
        json.set("messages",arr);
        //添加我们需要输入的内容
//        json.set("prompt", msg);
        json.set("temperature", 0);
//        json.set("max_tokens", 4096);
        json.set("top_p", 0.1);
        json.set("frequency_penalty", 0.0);
        json.set("presence_penalty", 0.6);
//        HttpResponse response = HttpRequest.post("https://api.openai.com/v1/completions")
        HttpResponse response = HttpRequest.post("https://api.openai.com/v1/chat/completions")
                .headerMap(headers, false)
                .bearerAuth("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")
                .body(String.valueOf(json))
                .timeout(5 * 60 * 1000)
                .execute();
        JSONArray choices = JSON.parseObject(response.body()).getJSONArray("choices");
        //提取回复的答案
        Object content = choices.getJSONObject(0).getJSONObject("message").get("content");
        //存入问题与回复
        ChatgptConversation chatgptConversation = new ChatgptConversation();
        chatgptConversation.setUserId(loginUser.getUserId());
        chatgptConversation.setTime(new Date());
        chatgptConversation.setQuestion(msg);
        chatgptConversation.setReply(content.toString());
        chatgptConversationService.save(chatgptConversation);
        return content;
    }

    @Override
    public List<ChatgptConversation> query(HttpServletRequest req) {
        //获取当前登录用户
        LoginUser loginUser = tokenService.getLoginUser(req);
        LambdaQueryWrapper<ChatgptConversation> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ChatgptConversation::getUserId,loginUser.getUserId());
        return chatgptConversationService.list(lqw);
    }
}
