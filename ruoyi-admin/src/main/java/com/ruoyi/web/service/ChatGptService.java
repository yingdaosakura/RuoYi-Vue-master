package com.ruoyi.web.service;


import com.ruoyi.web.entity.ChatgptConversation;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ChatGptService {

    public Object chat(HttpServletRequest req, String msg);

    List<ChatgptConversation> query(HttpServletRequest req);
}
