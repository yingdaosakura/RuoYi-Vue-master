package com.ruoyi.web.service;

import com.ruoyi.web.entity.ChatgptConversation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yangyuhang
* @description 针对表【chatgpt_conversation】的数据库操作Service
* @createDate 2023-05-03 19:13:05
*/
public interface ChatgptConversationService extends IService<ChatgptConversation> {

}
