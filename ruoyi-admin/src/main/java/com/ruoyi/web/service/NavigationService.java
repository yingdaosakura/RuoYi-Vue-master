package com.ruoyi.web.service;

import com.ruoyi.web.entity.Navigation;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
* @author yangyuhang
* @description 针对表【navigation】的数据库操作Service
* @createDate 2023-04-26 11:14:35
*/
public interface NavigationService extends IService<Navigation> {
    public String uploadFileAvatar(MultipartFile file);

}
