package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.web.entity.ChatgptConversation;
import com.ruoyi.web.service.ChatgptConversationService;
import com.ruoyi.web.mapper.ChatgptConversationMapper;
import org.springframework.stereotype.Service;

/**
* @author yangyuhang
* @description 针对表【chatgpt_conversation】的数据库操作Service实现
* @createDate 2023-05-03 19:13:05
*/
@Service
public class ChatgptConversationServiceImpl extends ServiceImpl<ChatgptConversationMapper, ChatgptConversation>
    implements ChatgptConversationService{

}




