package com.ruoyi.web.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.AliUtil;
import com.ruoyi.web.entity.Navigation;
import com.ruoyi.web.service.NavigationService;
import com.ruoyi.web.mapper.NavigationMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author yangyuhang
 * @description 针对表【navigation】的数据库操作Service实现
 * @createDate 2023-04-26 11:14:35
 */
@Service
public class NavigationServiceImpl extends ServiceImpl<NavigationMapper, Navigation>
        implements NavigationService {
    @Value("${oss.bucket}")
    public String bucket;
    @Value("${oss.accessKeyId}")
    public String accessKeyId;

    @Value("${oss.accessKeySecret}")
    public String accessKeySecret;

    @Value("${oss.endpoint}")
    public String endpoint;

    @Override
    public String uploadFileAvatar(MultipartFile file) {
        String url = null;

        //创建OSSClient实例。
        OSS ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        //获取上传文件输入流
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //获取文件名称
        String fileName = file.getOriginalFilename();

        //保证文件名唯一，去掉uuid中的'-'
        String uuid = UUID.randomUUID().toString().replaceAll("-", "").replaceAll("\\(","").replaceAll("\\(","");
        fileName = uuid + fileName;
        System.out.println(fileName);

        //把文件按日期分类，构建日期路径：avatar/2019/02/26/文件名
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String datePath = sdf.format(new Date());
        //拼接
        fileName = datePath + "/" + fileName;

        //调用oss方法上传到阿里云
        //第一个参数：Bucket名称
        //第二个参数：上传到oss文件路径和文件名称
        //第三个参数：上传文件输入流
        ossClient.putObject(bucket, fileName, inputStream);

        //把上传后把文件url返回
//        url = "https://" + bucket + "." + endpoint + "/" + fileName;
        //关闭OSSClient
        ossClient.shutdown();


        // 设置URL过期时间为1小时。
        Date expiration = new Date(System.currentTimeMillis() + 3600 * 1000);
        url = ossClient.generatePresignedUrl(bucket, fileName, expiration).toString();

        return url;
    }


}




