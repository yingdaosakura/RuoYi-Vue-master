package com.ruoyi.framework.config.properties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 根据环境，读取阿里云配置环境信息
 * admin 2021/8/16 09:47
 */
@Component
public class AliYunProperties {

    public static String accessKeyId;

    public static String accessKeySecret;

    public static String endpoint;

    @Value("${oss.accessKeyId}")
    public void setAccessKeyId(String accessKeyId) {
        AliYunProperties.accessKeyId = accessKeyId;
    }

    @Value("${oss.accessKeySecret}")
    public void setAccessKeySecret(String accessKeySecret) {
        AliYunProperties.accessKeySecret = accessKeySecret;
    }

    @Value("${oss.endpoint}")
    public void setEndpoint(String endpoint) {
        AliYunProperties.endpoint = endpoint;
    }

}
